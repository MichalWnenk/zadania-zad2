# -*- coding: utf-8 -*-

import mimetypes
import socket
import unittest
import sys


CRLF = '\r\n'
KNOWN_TYPES = set(mimetypes.types_map.values())


class ResponseOkTestCase(unittest.TestCase):
    """test jednostkowy dla metody `handle_client` i poprawnego żądania

    Test jednostkowy nie wymaga uruchomienia serwera.
    """

    def get_response(self):
        """uruchamia funkcję `handle_client` z modułu http_server"""
        from http_server import handle_client
        socket_stub = SocketStub('GET / HTTP/1.1\r\nHost: example.com\r\n\r\n')
        logger_stub = LoggerStub()
        handle_client(socket_stub, 'nic ciekawego', logger_stub)
        return socket_stub.response

    def test_response_code(self):
        ok = self.get_response()
        expected = "200 OK"
        actual = ok.split(CRLF)[0].split(' ', 1)[1].strip()
        self.assertEqual(expected, actual)

    def test_response_method(self):
        ok = self.get_response()
        expected = 'HTTP/1.1'
        actual = ok.split(CRLF)[0].split(' ', 1)[0].strip()
        self.assertEqual(expected, actual)

    def test_response_has_content_type_header(self):
        ok = self.get_response()
        headers = ok.split(CRLF+CRLF, 1)[0].split(CRLF)[1:]
        expected_name = 'content-type'
        has_header = False
        for header in headers:
            name, value = header.split(':')
            actual_name = name.strip().lower()
            if actual_name == expected_name:
                has_header = True
                break
        self.assertTrue(has_header)

    def test_response_has_legitimate_content_type(self):
        ok = self.get_response()
        headers = ok.split(CRLF+CRLF, 1)[0].split(CRLF)[1:]
        expected_name = 'content-type'
        for header in headers:
            name, value = header.split(':')
            actual_name = name.strip().lower()
            if actual_name == expected_name:
                self.assertTrue(value.strip() in KNOWN_TYPES)
                return
        self.fail('no content type header found')


class ResponseMethodNotAllowedTestCase(unittest.TestCase):
    """test jednostkowy dla metody `handle_client` i błędnej metody"""

    def get_response(self, method):
        """uruchamia funkcję `handle_client` z modułu http_server"""
        from http_server import handle_client
        socket_stub = SocketStub('{0} / HTTP/1.1\r\nHost: example.com\r\n\r\n'.format(method))
        logger_stub = LoggerStub()
        handle_client(socket_stub, 'nic ciekawego', logger_stub)
        return socket_stub.response

    def test_response_code(self):
        methods = ['POST', 'PUT', 'DELETE', 'HEAD']
        for method in methods:
            resp = self.get_response(method)
            expected = "405 Method Not Allowed"
            actual = resp.split(CRLF)[0].split(' ', 1)[1].strip()
            self.assertEqual(expected, actual)

    def test_response_method(self):
        resp = self.get_response('PUT')
        expected = 'HTTP/1.1'
        actual = resp.split(CRLF)[0].split(' ', 1)[0].strip()
        self.assertEqual(expected, actual)


class HTTPServerFunctionalTestCase(unittest.TestCase):
    """test funkcjonalny serwera HTTP

    Ten przypadek testowy wchodzi w interakcję z serwerem http
    i dlatego wymaga, by był on uruchomiony.
    """

    def send_message(self, message):
        """Próba wysłania żądania przy uzyciu testowego klienta.

        Informuje o problemie w przypadku błędu gniazda.
        """
        response = ''
        try:
            response = client(message)
        except socket.error, e:
            if e.errno == 61:
                msg = "Blad: {0}, czy serwer dziala?"
                self.fail(msg.format(e.strerror))
            else:
                self.fail("Niespodziewany blad: {0}".format(str(e)))
        return response

    def test_get_request(self):
        message = CRLF.join(['GET / HTTP/1.1', 'Host: example.com', ''])
        expected = '200 OK'
        actual = self.send_message(message)
        self.assertTrue(expected in actual)

    def test_post_request(self):
        message = CRLF.join(['POST / HTTP/1.1', 'Host: example.com', ''])
        expected = '405 Method Not Allowed'
        actual = self.send_message(message)
        self.assertTrue(expected in actual)


class SocketStub:

    def __init__(self, request):
        self.request = request
        self.response = ''
        self.offset = 0

    def recv(self, length):
        data = self.request[self.offset:self.offset+length]
        self.offset += length
        return data

    def send(self, response):
        self.response += response

    def sendall(self, response):
        self.response += response


class LoggerStub:

    def info(self, msg):
        pass


def client(msg):
    server_address = ('localhost', 6306)
    sock = socket.socket(
        socket.AF_INET, socket.SOCK_STREAM, socket.IPPROTO_TCP
    )
    print >>sys.stderr, u'podłączono do {0} na porcie {1}'.format(*server_address)
    sock.connect(server_address)
    response = ''
    done = False
    bufsize = 1024
    try:
        print >>sys.stderr, 'wysylanie "{0}"'.format(msg)
        sock.sendall(msg)
        while not done:
            chunk = sock.recv(bufsize)
            if len(chunk) < bufsize:
                done = True
            response += chunk
        print >>sys.stderr, 'otrzymano "{0}"'.format(response)
    finally:
        print >>sys.stderr, u'zamykanie połączenia'
        sock.close()
    return response


if __name__ == '__main__':
    unittest.main()

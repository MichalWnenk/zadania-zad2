# -*- coding: utf-8 -*-

import smtplib

def create_and_send_email():
    """Funkcja interaktywna pytająca użytkownika o potrzebne dane
    i wysyłająca na ich podstawie list elektroniczny.
    """
    # Odczyt danych od użytkownika
    nadawca = raw_input("Podaj nadawce :")
    adresat = raw_input("Podaj adresata :")
    temat = raw_input("Podaj temat :")
    tresc = raw_input("Podaj tresc :")

    # Stworzenie wiadomości

    template = "From: %s\r\nTo: %s\r\nSubject: %s\r\n\r\n"
    headers = template % (nadawca, adresat, temat)
    email_body = headers + tresc

    server = smtplib.SMTP('194.29.175.240', 25)

    try:
        # Połączenie z serwerem pocztowym
        server.set_debuglevel(True)
        server.ehlo()

        # Ustawienie parametrów
        server.has_extn('STARTTLS')

        # Autentykacja
        server.starttls()
        server.ehlo()
        server.login('p4', 'p4')

        # Wysłanie wiadomości
        server.sendmail(nadawca, [adresat], email_body)

    finally:
        # Zamknięcie połączenia
        server.close()


if __name__ == '__main__':
    decision = 't'
    while decision in ['t', 'T', 'y', 'Y']:
        create_and_send_email()
        decision = raw_input('Wysłać jeszcze jeden list? ')

# -*- coding: utf-8 -*-

import socket
import sys
import logging
import logging.config
import email.utils
from mimetypes import MimeTypes
import urllib
import os.path
import mimetypes


def handle_client(connection, logger):
    """Obsługa konwersacji HTTP z pojedynczym klientem

    connection: socket klienta
    html:       wczytana strona html do zwrócenia klientowi
    logger:     mechanizm do logowania wiadomości
    """
    # Odebranie żądania

    buffer = 1024
    request = ''
    done = False
    while not done:
        part = connection.recv(buffer)
        if len(part) < buffer:
            done = True
        request += part

    logger.info(u'odebrano: "{0}"'.format(request))

    #sprawdzanie żądania

    podzieloneZadanie = request.split(' ')
    czas = 'GMT Date: ' + email.utils.formatdate() + '\r\n'
    status = ''
    header = ''
    contentt=''
    contentl=''
    html = ''
    shorter = podzieloneZadanie[1][1:] # wyciaganie sciezki do stronki na servie
    stronka = 'web/'+shorter

    #if not stronka.endswith('.html'):
     #   mime = MimeTypes()
     #   url = urllib.pathname2url(stronka)
     #   html = open(mime.guess_type(url)).read()
    #else:

    #sprawdzanie do czego prowadzi sciezka
    if os.path.isfile(stronka):
        if stronka.endswith('.html') or stronka.endswith('.txt'):
            html = open(stronka).read()
            contentt = mimetypes.guess_type(stronka)[0]
            contentl = str(len(html))
        else:
            if stronka.endswith('.jpg') or stronka.endswith('.png'):
                html = open(stronka, 'rb').read()
                contentt = mimetypes.guess_type(stronka)[0]
                contentl = str(len(html))
            else:
                html = "Nie obsługiwany format pliku!"
                print "Nie obsługiwany format pliku!"
    elif os.path.isdir(stronka):
        listFiles = os.listdir(stronka)
        html += """
        <!DOCTYPE html>
        <html>
            <body>
                <table>
                        Pliki i podkatalogi:
                """
        for p in listFiles:
            html += """
            <tr><td><a href="%s">%s</a></td></tr>
                    """ % (shorter+'/'+str(p), str(p))

        html += """
                </table></body></html>
                """
    else:
        html = 'HTTP/1.1 404 NOT FOUND \r\n'

    if (podzieloneZadanie[0]) == 'GET':
        if (podzieloneZadanie[2])[:4] == 'HTTP':
            if html:
                #zapytanie jest zgodne z http
                status = 'HTTP/1.1 200 OK \r\n'

            else:
                status = 'HTTP/1.1 404 NOT FOUND \r\n'

        else:
            status = 'HTTP/1.1 403 BAD REQUEST \r\n'

    else:
        status = 'HTTP/1.1 405 Method Not Allowed \r\n'
        html = ''

    try:
        if contentt == '' and contentl == '':
            header = status
        else:
            header = status \
             + 'Content-Type: ' + contentt+ '; charset=UTF-8' + '\r\n' \
             + 'Content-Length: ' + contentl + '\r\n' \

        # Wysłanie zawartości strony
        print(header + czas + html + '\r\n')
        connection.sendall(header + czas + '\r\n' + html + '\r\n')
        logger.info(u'wysyłano odpowiedź')
    except:
        header = 'HTTP/1.1 500 Internal Server Error \r\n'
        print(header + czas + '\r\n')
        connection.sendall(header + czas + '\r\n' + "500 Internal Server Error" + '\r\n')

def http_serve(server_socket, logger):
    """Obsługa połączeń HTTP

    server_socket:  socket serwera
    html:           wczytana strona html do zwrócenia klientowi
    logger:         mechanizm do logowania wiadomości
    """
    try:
        while True:
            # Czekanie na połączenie
            connection, client_address = server_socket.accept()
            logger.info(u'połączono z {0}:{1}'.format(*client_address))

            try:
                handle_client(connection, logger)

            finally:
                # Zamknięcie połączenia
                connection.close()
    except KeyboardInterrupt:
        server_socket.close()


def server(logger):
    """Server HTTP

    logger: mechanizm do logowania wiadomości
    """
    # Tworzenie gniazda TCP/IP
    server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    # Ustawienie ponownego użycia tego samego gniazda
    server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

    # Powiązanie gniazda z adresem 194.29.175.240
    server_address = ('localhost', 6306)
    server_socket.bind(server_address)
    logger.info(u'uruchamiam server na {0}:{1}'.format(*server_address))

    # Nasłuchiwanie przychodzących połączeń
    server_socket.listen(1)

    try:
        http_serve(server_socket, logger)

    finally:
        server_socket.close()


if __name__ == '__main__':
    logging.config.fileConfig('logging.conf')
    logger = logging.getLogger('full_http_server')
    server(logger)
    sys.exit(0)